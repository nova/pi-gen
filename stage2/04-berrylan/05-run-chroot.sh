#!/bin/bash -e
echo "* * * * * bash /root/install.sh" > /root/cronjob

crontab /root/cronjob
echo "added cronjob"
crontab -l



cat <<EOM >/root/install.sh
#!/bin/bash
# set date to UTC
timedatectl set-timezone UTC

# I don't know why but nymea-manager fails if the pi reboots even once. restarting the service fixes it somehow
if ! (($(date | cut -d : -f 2) % 2)); then 
  systemctl restart nymea-networkmanager.service
fi

# IS THIS REAL? check if service is installed
systemctl is-active --quiet deviceplane-agent.service && echo deviceplane running >> /root/logs && crontab -r

case "$(curl -k -s --max-time 3 -I http://google.com | sed 's/^[^ ]*  *\([0-9]\).*/\1/; 1q')" in
  [23]) echo "HTTP connectivity is up" >> /root/logs && cat /root/deviceplane.sh | VERSION=1.16.0 PROJECT=prj_1gh85hT2F6qmPZryKM9b2ib5Zsd REGISTRATION_TOKEN=drt_1gh85ef2BtrXa7fcIHmxSmgdQ9c CONTROLLER=https://deviceplane.novachat-helsinki.kubesail-ops.com/api bash ;;
  5) echo "The web proxy won't let us through" >> /root/logs;;
  *) echo "The network is down or very slow" >> /root/logs ;;
esac

date >> /root/logs
EOM
chmod 755 /root/install.sh

cat <<EOM >/etc/nymea/nymea-networkmanager.conf
[General]
Mode=offline
Timeout=120
AdvertiseName=NovaChat Bridgebox
PlatformName=bridgebox
ButtonGpio=14
EOM

apt remove iptables -y 
apt install nftables -y

cat <<EOM >/root/pi-gen
0.1.2
EOM

echo "set up files"
