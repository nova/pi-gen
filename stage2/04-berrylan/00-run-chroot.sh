#!/bin/bash -e
# Add nymea repository
echo -e "\n## nymea repo\ndeb http://repository.nymea.io buster main\n#deb-src http://repository.nymea.io buster main" | tee /etc/apt/sources.list.d/nymea.list
apt-key adv --keyserver keyserver.ubuntu.com --recv-key A1A19ED6

apt-get update
apt-get upgrade

systemctl enable ssh
systemctl disable dhcpcd5
